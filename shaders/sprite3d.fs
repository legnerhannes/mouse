#version 330 core
layout(location = 0) out vec4 FragColor;
layout(location = 3) out vec4 SpriteValue;

in vec2 UV;

uniform sampler2D sprite;

void main()
{
    vec4 color = texture(sprite, UV);
    FragColor = color;

    if (color.w > 0.0) SpriteValue = vec4(1.0, 1.0, 1.0, 1.0);
    else SpriteValue = vec4(0.0, 0.0, 0.0, 1.0);
}