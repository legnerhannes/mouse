#version 330 core
layout(location = 0) out vec4 FragColor;
layout(location = 1) out vec4 NormalColor;
layout(location = 2) out vec4 DepthValue;
layout(location = 3) out vec4 SpriteValue;

in vec3 Normal;
in vec2 UV;

uniform int drawOL;
uniform vec3 lightDir, cameraDir;
uniform sampler2D texture_diffuse1;

void main()
{
    // render normals
    if (drawOL == 0) NormalColor = vec4(Normal, 1.0);
    DepthValue = vec4(gl_FragCoord.z, gl_FragCoord.z, gl_FragCoord.z, 1.0);
    SpriteValue = vec4(0.0, 0.0, 0.0, 1.0);
    //return;

    vec3 camera = normalize(cameraDir);
    vec3 normal = normalize(Normal);

    vec3 color = vec3(1.0, 1.0, 1.0);
    vec3 lightColor = vec3(1.0, 1.0, 1.0);

        
    vec3 light = normalize(lightDir);

    float diff = max(dot(normal, light), 0.0);
    vec3 diffuse = diff * lightColor;

    if (diff > 0.6) diffuse = lightColor;
    else if (diff > 0.3) diffuse = 0.7 * lightColor;
    else diffuse = 0.4 * lightColor;
                    

    vec4 texColor = texture(texture_diffuse1, UV);
    vec3 result = (vec3(0.5) + diffuse) * color * vec3(texColor.x, texColor.y, texColor.z);

    float cosAngle = dot(normal, camera);
    float cosThreshold = cos(3.0);

    //if (cosAngle < cosThreshold) FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    //else FragColor = vec4(result, 1.0);
    FragColor = vec4(result, 1.0);
    
    //FragColor = texColor;
}