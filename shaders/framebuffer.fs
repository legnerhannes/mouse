#version 330 core
out vec4 FragColor;
  
in vec2 TexCoords;

uniform sampler2D colorTex, normalTex, depthTex, spriteTex;
uniform float width;
uniform float height;

float[9] sobel0 = float[] ( 
    1.0, 2.0, 1.0, 
    0.0, 0.0, 0.0, 
   -1.0, -2.0, -1.0 
);
float[9] sobel1 = float[] ( 
    1.0, 0.0, -1.0, 
    2.0, 0.0, -2.0, 
    1.0, 0.0, -1.0 
);

void getColors(inout vec3 colors[9], sampler2D tex, vec2 coord)
{
	float w = 1.0 / width;
	float h = 1.0 / height;

	colors[0] = texture(tex, coord + vec2( -w, -h)).rbg;
	colors[1] = texture(tex, coord + vec2(0.0, -h)).rbg;
	colors[2] = texture(tex, coord + vec2(  w, -h)).rbg;
	colors[3] = texture(tex, coord + vec2( -w, 0.0)).rbg;
	colors[4] = texture(tex, coord).rbg;
	colors[5] = texture(tex, coord + vec2(  w, 0.0)).rbg;
	colors[6] = texture(tex, coord + vec2( -w, h)).rbg;
	colors[7] = texture(tex, coord + vec2(0.0, h)).rbg;
	colors[8] = texture(tex, coord + vec2(  w, h)).rbg;
}

void main()
{ 
    // FragColor = texture(normalTex, TexCoords);
    // return;

    vec3 colors[9];
	getColors(colors, normalTex, TexCoords.st);

	vec3 value0 = vec3(0.0);
    vec3 value1 = vec3(0.0);
    for (int i = 0; i < 9; i++) {
        value0 += colors[i] * sobel0[i];
        value1 += colors[i] * sobel1[i];
    }

    vec3 maxVec = max(value0, value1);
    bool sprite = texture(spriteTex, TexCoords).x == 1.0;

    if (length(maxVec) > 1.0 && !sprite) FragColor = vec4(texture(colorTex, TexCoords).rbg * 0.0, 1.0);
    else FragColor = texture(colorTex, TexCoords);
}