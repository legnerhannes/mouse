#include "game_object.hpp"
#include "util.hpp"
#include "i_res.hpp"

#include <iostream>

void SpriteObject::render() {
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, m_position);
    
    Res::getCanvas("30x30").draw(model, m_sprite);

    //Util::spriteRenderer -> DrawSprite(m_sprite, glm::vec2(m_position.x, m_position.z), m_size, 0.0f, m_color);
}

void SpriteObject::setRenderMatrices(glm::mat4 view, glm::mat4 proj) {
   Res::getCanvas("30x30").setMatrices(view, proj);
}


void MeshObject::setRenderMatrices(glm::mat4 view, glm::mat4 proj) {
    Res::getModel(m_model).setMatrices(view, proj);
}

void MeshObject::render() {
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::scale(model, m_scale);
    model = glm::translate(model, m_position);

    if (m_rotation.x != 0.0f)  model = glm::rotate(model, m_rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
    if (m_rotation.y != 0.0f)  model = glm::rotate(model, m_rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
    if (m_rotation.z != 0.0f)  model = glm::rotate(model, m_rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

    Res::getModel(m_model).draw(model);
}

Prop::Prop(std::string id, std::string modelPath, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, unsigned int tex, std::vector<Collider> collider) {

    m_model = modelPath;

    m_id = id;
    m_position = position;
    m_scale = scale;
    m_rotation = rotation;
    m_collider = collider;
}