#ifndef COLLIDER_HPP
#define COLLIDER_HPP

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "i_res.hpp"

enum Direction {
	UP,
	RIGHT,
	DOWN,
	LEFT
};  

typedef std::tuple<bool, Direction, glm::vec2> Collision;

class Collider {
    private:
        std::string m_type;
        unsigned int m_quadVAO;
        glm::mat4 m_proj, m_view, m_model;

    public:
        glm::vec2 m_topLeft, m_bottomRight, m_offset;
        
        Collider() {};
        Collider(std::string type, glm::vec2 topLeft, glm::vec2 bottomRight);

        std::string getType();
        bool isColliding(glm::vec2 thisOffset, Collider other, glm::vec2 otherOffset, glm::vec2* resolution);
        void drawCollider();
        void setMatrices(glm::mat4 projection, glm::mat4 view, glm::mat4 model);
};

#endif