#include "player.hpp"
#include "input.hpp"
#include "i_res.hpp"
#include "game.hpp"

#include <iostream>

Player::Player(glm::vec3 position, glm::vec2 size, unsigned int sprite, std::string canvasName) {
    m_position = position;
    m_size = size;
    m_canvas = canvasName;
    
    m_sprite = sprite;
    m_color = glm::vec3(1.0f, 1.0f, 1.0f);

    Animation walkDown("labrat", 0, 3);
    walkDown.addSprite("labrat0");
    walkDown.addSpriteBatch("labrat", 4, 6);
    m_animations.insert(std::pair<std::string, Animation>("walkDown", walkDown));

    Animation walkUp("labrat", 7, 10);
    walkUp.addSprite("labrat7");
    walkUp.addSpriteBatch("labrat", 11, 13);
    m_animations.insert(std::pair<std::string, Animation>("walkUp", walkUp));

    Animation walkRight("labrat", 14, 17);
    walkRight.addSprite("labrat14");
    walkRight.addSpriteBatch("labrat", 18, 20);
    m_animations.insert(std::pair<std::string, Animation>("walkRight", walkRight));

    Animation walkLeft("labrat", 21, 24);
    walkLeft.addSprite("labrat21");
    walkLeft.addSpriteBatch("labrat", 25, 27);
    m_animations.insert(std::pair<std::string, Animation>("walkLeft", walkLeft));


    m_direction = down;
    currentState = enterIdle();

    m_collider.push_back(Collider("hurtbox", glm::vec2(-0.3f, 0.4f), glm::vec2(0.3f, -0.2f)));
}


state Player::enterIdle() {
    return idle;
}

void Player::updateIdle() {
    if (Input::up || Input::down || Input::left || Input::right) currentState = enterWalk();
}


state Player::enterWalk() {
    m_animations["walkUp"].restartAnimation(4);
    m_animations["walkDown"].restartAnimation(4);
    m_animations["walkLeft"].restartAnimation(4);
    m_animations["walkRight"].restartAnimation(4);
    
    // necessary to prevent backwards walk
    if (Input::up) m_direction = up;
    if (Input::down) m_direction = down;
    if (Input::left) m_direction = left;
    if (Input::right) m_direction = right;

    return walk;
}

void Player::updateWalk() {
    glm::vec2 velocity = glm::vec2(0.0f, 0.0f);

    if (Input::up) {
        velocity.y += 1.0f;
    }
    if (Input::down) {
        velocity.y -= 1.0f;
    }
    if (Input::left) {
         velocity.x -= 1.0f;
    }
    if (Input::right) {
        velocity.x += 1.0f;
    }
    
    
    if (glm::length(velocity) > 0.0f) {
        velocity = glm::normalize(velocity) * 0.01f * SPEED;
        m_position.x += velocity.x;
        m_position.y += velocity.y;

        if (velocity.y > 0.0f && velocity.x == 0.0f) m_direction = up;
        if (velocity.y < 0.0f && velocity.x == 0.0f) m_direction = down;
        if (velocity.x > 0.0f && velocity.y == 0.0f) m_direction = right;
        if (velocity.x < 0.0f && velocity.y == 0.0f) m_direction = left;

        if (m_direction == up) m_sprite = m_animations["walkUp"].getCurrentSprite();
        if (m_direction == down) m_sprite = m_animations["walkDown"].getCurrentSprite();
        if (m_direction == right) m_sprite = m_animations["walkRight"].getCurrentSprite();
        if (m_direction == left) m_sprite = m_animations["walkLeft"].getCurrentSprite();

    }
    else currentState = enterIdle();

    m_animations["walkUp"].updateAnimation();
    m_animations["walkDown"].updateAnimation();
    m_animations["walkLeft"].updateAnimation();
    m_animations["walkRight"].updateAnimation();
}


unsigned int Player::getCurrentSprite() {
    return 1;
}

void Player::checkCollision(std::vector<Prop> objects) {
    // iterates all objects in scene for collider
    for (Prop object : objects) {
        for (Collider collider : object.m_collider) {
            if (collider.getType() == "terrain") {
                glm::vec2 res;
                if (m_collider[0].isColliding(glm::vec2(m_position.x, m_position.y), collider, glm::vec2(object.m_position.x, object.m_position.y), &res)) {
                    m_position = glm::vec3(m_position.x + res.x * 1.01f, m_position.y + res.y * 1.01f, m_position.z);
                };
            }
        }
    }
}


void Player::update() {
    switch (currentState) {
        case idle:
            updateIdle();
            break;

        case walk:
            updateWalk();
            break;
        
        default:
            break;
    }
}