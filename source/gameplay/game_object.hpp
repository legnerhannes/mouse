#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "i_res.hpp"
#include "collider.hpp"


class GameObject {
    public:
        std::string m_id;
        glm::vec3 m_position, m_color;
        virtual void render() = 0;
        std::vector<Collider> m_collider;
};

class SpriteObject : public GameObject {
    protected:
        unsigned int m_sprite;
        std::string m_canvas;

    public:
        glm::vec2 m_size;
        void setRenderMatrices(glm::mat4 view, glm::mat4 proj);
        void render();
};

class MeshObject : public GameObject {
    protected:
        glm::vec3 m_scale, m_rotation;

    public:
        std::string m_model;
        void setRenderMatrices(glm::mat4 view, glm::mat4 proj);
        void render();
};

class Prop : public MeshObject {

    public:
        Prop(std::string id, std::string modelPath, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, unsigned int tex,  std::vector<Collider> collider);
};

#endif