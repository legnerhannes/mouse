#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "game_object.hpp"
#include "animation.hpp"
#include "collider.hpp"

#include <string>

enum direction {
    up,
    down,
    left,
    right
};

enum state {
    idle = 0,
    walk = 1
};


class Player : public SpriteObject {
    private:
        const float SPEED = 5.0f;

        state currentState;
        direction m_direction;

        std::map<std::string, Animation> m_animations;

        state enterIdle();
        void updateIdle();

        state enterWalk();
        void updateWalk();

        unsigned int getCurrentSprite();

    public:
        Player(glm::vec3 position, glm::vec2 size, unsigned int sprite, std::string canvasName);

        void update();
        void checkCollision(std::vector<Prop> objects);
};

#endif