#include "animation.hpp"

Animation::Animation(std::string sprite) {
    addSprite(sprite);
}

Animation::Animation(std::string sprite, int start, int end) {
    addSpriteBatch(sprite, start, end);
}

void Animation::addSprite(std::string sprite) {
    m_sprites.push_back(sprite);
}

void Animation::addSpriteBatch(std::string sprite, int start, int end) {
    for (int i = start; i <= end; i++) {
        m_sprites.push_back(sprite + std::to_string(i));
    }
}

void Animation::restartAnimation(int framesPerSprite) {
    m_framesPerSprite = framesPerSprite;
    m_currentSprite = 0;
    m_currentSpriteFrames = 0;
}

void Animation::updateAnimation() {
    m_currentSpriteFrames++;

    if (m_currentSpriteFrames >= m_framesPerSprite) {
        m_currentSprite++;
        m_currentSpriteFrames = 0;
    }

    if (m_currentSprite >= m_sprites.size()) {
        m_currentSprite = 0;
    }
}

unsigned int Animation::getCurrentSprite() {
    return Res::getTexture(m_sprites[m_currentSprite]).ID;
}