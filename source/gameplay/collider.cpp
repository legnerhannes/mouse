#include "collider.hpp"

#include <iostream>

Collider::Collider(std::string type, glm::vec2 topLeft, glm::vec2 bottomRight) 
    : m_type(type), m_topLeft(topLeft), m_bottomRight(bottomRight) {


        float colliderVertices[] = {
            bottomRight.x, topLeft.y, 0.0f,
            bottomRight.x, bottomRight.y, 0.0f,
            topLeft.x, bottomRight.y, 0.0f, 

            bottomRight.x, topLeft.y, 0.0f,
            topLeft.x, bottomRight.y, 0.0f,
            topLeft.x, topLeft.y, 0.0f
        };

        unsigned int VBO;

        glGenVertexArrays(1, &m_quadVAO);
        glGenBuffers(1, &VBO);

        glBindVertexArray(m_quadVAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(colliderVertices), &colliderVertices, GL_STATIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*) 0);
}

std::string Collider::getType() {
    return m_type;
}

bool Collider::isColliding(glm::vec2 thisOffset, Collider other, glm::vec2 otherOffset, glm::vec2* resolution) {
    float thisLeft = m_topLeft.x + thisOffset.x;
    float thisRight = m_bottomRight.x + thisOffset.x;
    float thisTop = m_topLeft.y + thisOffset.y;
    float thisBot = m_bottomRight.y + thisOffset.y;

    float otherLeft = other.m_topLeft.x + otherOffset.x;
    float otherRight = other.m_bottomRight.x + otherOffset.x;
    float otherTop = other.m_topLeft.y + otherOffset.y;
    float otherBot = other.m_bottomRight.y + otherOffset.y;

    bool collisionX = thisLeft <= otherRight && thisRight >= otherLeft;
    bool collisionY = thisBot <= otherTop && thisTop >= otherBot;
    
    if (collisionX && collisionY) {
        if (std::abs(thisLeft - otherRight) >= std::abs(thisRight - otherLeft)) resolution -> x = otherLeft - thisRight;
        else resolution -> x = otherRight - thisLeft;

        if (std::abs(thisBot - otherTop) >= std::abs(thisTop - otherBot)) resolution -> y = otherBot - thisTop;
        else  resolution -> y = otherTop - thisBot;
        
        if (std::abs(resolution -> x) > std::abs(resolution -> y)) resolution -> x = 0.0f;
        else resolution -> y = 0.0f;

        return true;
    }
    else return false;
}

void Collider::setMatrices(glm::mat4 projection, glm::mat4 view, glm::mat4 model) {
    m_proj = projection;
    m_view = view;
    m_model = model;
}


void Collider::drawCollider() {
    Res::getShader("collider_shader").Use();
    Res::getShader("collider_shader").SetMatrix4("projection", m_proj);
    Res::getShader("collider_shader").SetMatrix4("view", m_view);
    Res::getShader("collider_shader").SetMatrix4("model", m_model);
    glBindVertexArray(m_quadVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6); 
}