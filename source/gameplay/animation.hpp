#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include "i_res.hpp"

#include <string>
#include <vector>
#include <map>

class Animation {
    private:
        int m_framesPerSprite, m_currentSprite, m_currentSpriteFrames;
        std::vector<std::string> m_sprites;

    public:
        Animation() {};
        Animation(std::string sprite);
        Animation(std::string sprite, int start, int end);

        void addSprite(std::string sprite);
        void addSpriteBatch(std::string sprite, int start, int end);
        void restartAnimation(int framesPerSprite);
        void updateAnimation();
        unsigned int getCurrentSprite();

};

#endif