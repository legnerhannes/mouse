#include "util.hpp"

template <typename T>
std::vector<T> Util::getVectorFromArray(T* array, int size) {
    std::vector<T> output;

    for (int i = 0; i < size; i++) {
        output.push_back(array[i]);
    }

    return output;
}