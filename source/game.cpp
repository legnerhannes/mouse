/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#include "game.hpp"
#include "i_res.hpp"
#include "input.hpp"
#include "util.hpp"

#include <iostream>


Game::Game(unsigned int width, unsigned int height) 
    : State(GAME_ACTIVE), Keys(), m_width(width), m_height(height)
{ 

}

Game::~Game()
{
    Res::clearResources();
    delete m_player;
}


void Game::processInput() {

    if (Input::interact) {
        m_camPos += glm::vec3(0.0f, 0.0f, 0.01f);
        SpriteCanvas::setCameraAngle(m_camPos - m_lookAt);
    }

    if (Input::attack) {
        m_camPos += glm::vec3(0.0f, 0.0f, -0.01f);
        SpriteCanvas::setCameraAngle(m_camPos - m_lookAt);
    }
}

void Game::render() {
    
    glViewport(0, 0, Res::getFramebuffer("framebuffer").getWidth(), Res::getFramebuffer("framebuffer").getHeight()); // change viewport so the whole thing can be rendered in the low resolution framebuffer

    //Texture2D labratTexture = ResourceManager::GetTexture("labrat");
    //m_renderer -> DrawSprite(labratTexture, glm::vec2(200.0f, 200.0f), glm::vec2(100.0f, 100.0f), 0.0f, glm::vec3(1.0f, 1.0f, 1.0f));

    glm::mat4 view = glm::lookAt(m_camPos, m_lookAt, glm::vec3(0.0f, 1.0f, 0.0f));
    //glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float) m_width / m_height, 0.1f, 100.0f);
    float orthoScale = 0.005f;
    //m_unit = (m_width * orthoScale * 2) / Util::GAME_WIDTH;
    glm::mat4 proj = glm::ortho(-(float) m_width * orthoScale, (float) m_width * orthoScale, -(float) m_height * orthoScale, (float) m_height * orthoScale, 0.1f, 50.0f);  //glm::ortho(-m_width / 2, m_width / 2, m_height / 2, -m_height / 2);
    //glm::mat4 proj = glm::ortho(-(float) m_width / 2.0f, (float) m_width / 2.0f, -(float) m_height / 2.0f, (float) m_height / 2.0f, 0.1f, 30.0f);

    for (int i = 0; i < m_props.size(); i++) {
        m_props[i].setRenderMatrices(view, proj);
        m_props[i].render();

        // TODO: CHANGE HOW COLLIDER ARE DRAWN

        for (Collider& collider : m_props[i].m_collider) {
            collider.setMatrices(proj, view, glm::translate(glm::mat4(1.0f), glm::vec3(m_props[i].m_position.x, m_props[i].m_position.y, 0.0f)));
        }
    }
    
    m_player -> setRenderMatrices(view, proj);
    m_player -> render();

    
    for (Collider& collider : m_player->m_collider) {
        collider.setMatrices(proj, view, glm::translate(glm::mat4(1.0f), glm::vec3(m_player->m_position.x, m_player->m_position.y, 0.0f)));
    }
    
    glViewport(0, 0, m_width, m_height);
}   



void Game::init() {

    // load shaders
    Res::createShader("../shaders/sprite.vs", "../shaders/sprite.fs", nullptr, "sprite_shader");
    Res::createShader("../shaders/sprite3d.vs", "../shaders/sprite3d.fs", nullptr, "sprite3d_shader");
    Res::createShader("../shaders/model.vs", "../shaders/model.fs", nullptr, "model_shader");
    Res::createShader("../shaders/framebuffer.vs", "../shaders/framebuffer.fs", nullptr, "framebuffer_shader");
    Res::createShader("../shaders/collider.vs", "../shaders/collider.fs", nullptr, "collider_shader");

    // configure sprite shaders
    glm::mat4 projection = glm::ortho(0.0f, static_cast<float>(this->m_width), 
        static_cast<float>(this->m_height), 0.0f, -1.0f, 1.0f);
    Res::getShader("sprite_shader").Use().SetInteger("image", 0);
    Res::getShader("sprite_shader").SetMatrix4("projection", projection);

    // setup cam
    m_lookAtStart = glm::vec3(0.0f, 0.0f, 0.0f);
    m_lookAt = m_lookAtStart;

    m_camPosStart = glm::vec3(0.0f, -10.0f, 10.0f);
    m_camPos = m_camPosStart;

    Res::getShader("model_shader").Use().SetVector3f("cameraDir", m_lookAt - m_camPos);
    SpriteCanvas::setCameraAngle(m_camPos - m_lookAt);

    // load textures
    Res::createFramebuffer("framebuffer_shader", Util::SCREEN_WIDTH, Util::SCREEN_HEIGHT, "framebuffer"); // alttp res 256 × 224  -> 256 x 144 for 16:9 -> *1.5 = 384 x 216

    Res::createSprites("../resources/sprites/firstSpritesheet.png", 7, 4, 30, 30, "labrat");

    Res::createTexture("../resources/sprites/labrat.png", true, GL_NEAREST, GL_CLAMP_TO_EDGE, "labrat");
    Res::createTexture("../resources/models/appleOneColor.png", true, GL_LINEAR, GL_CLAMP_TO_EDGE, "apple");
    Res::createTexture("../resources/models/doseTexture.png", true, GL_LINEAR, GL_CLAMP_TO_EDGE, "dose");
    Res::createTexture("../resources/models/wallTexture.png", true, GL_LINEAR, GL_CLAMP_TO_EDGE, "wall");
    Res::createTexture("../resources/models/groundTexture.png", true, GL_LINEAR, GL_CLAMP_TO_EDGE, "ground");
    Res::createTexture("../resources/models/brickTexture.png", true, GL_LINEAR, GL_CLAMP_TO_EDGE, "brick");

    Res::createTexture("../resources/models/grasTexture.png", true, GL_LINEAR, GL_CLAMP_TO_EDGE, "gras");
    Res::createTexture("../resources/models/levelTexture.png", true, GL_LINEAR, GL_CLAMP_TO_EDGE, "level");


    Res::createModel("../resources/models/ground.ply", "model_shader", "ground", "ground");
    Res::createModel("../resources/models/wall.ply", "model_shader", "wall", "wall");
    Res::createModel("../resources/models/apple.ply", "model_shader", "apple", "apple");
    Res::createModel("../resources/models/dose.ply", "model_shader", "dose", "dose");
    Res::createModel("../resources/models/brick.ply", "model_shader", "brick", "brick");

    Res::createModel("../resources/models/grasses.ply", "model_shader", "gras", "grasses");
    Res::createModel("../resources/models/grasLevel.ply", "model_shader", "level", "level");


    Res::createCanvas(30, "sprite3d_shader", "30x30");

    // Res::createLevel("../resources/level/testLevel.json", "test_level");
    Res::createLevel("../resources/level/grasLevel.json", "gras_level");

    // configure game objects (player)
    glm::vec2 playerPos = glm::vec2(m_width / 2.0f, m_height / 2);
    m_player = new Player(glm::vec3(0.0f, 0.0f, 0.3f), glm::vec2(30.0f, 30.0f), Res::getTexture("labrat0").ID, "30x30");
    m_colliderInScene.push_back(m_player->m_collider[0]);


    // add props
    int id = 0;
    for (PropData data : Res::getLevel("gras_level").m_props) {
        std::vector<Collider> collider;
        
        for (glm::vec4 coords : data.collider) {
            Collider coll("terrain", glm::vec2(coords.x, coords.y), glm::vec2(coords.z, coords.w));
            collider.push_back(coll);
            m_colliderInScene.push_back(coll);
        }

        m_props.push_back(Prop(std::to_string(id), data.modelName, data.position, data.scale, data.rotation, Res::getTexture(data.texName).ID, collider));

        id++;
    }
}

void Game::update() {

    // manage user input
    // -----------------
    processInput();

    // render
    // ------
    Res::getFramebuffer("framebuffer").bindFramebuffer();

    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    m_player -> update();
    m_player -> checkCollision(m_props);
    
    m_camPos = m_camPosStart + m_player->m_position;
    m_lookAt = m_lookAtStart + m_player->m_position;
    render();


    glClear(GL_DEPTH_BUFFER_BIT);
    for (Collider collider : m_player->m_collider) {
        collider.drawCollider();
    }


    for (int i = 0; i < m_props.size(); i++) {
        for (Collider collider : m_props[i].m_collider) {
            collider.drawCollider();
        }
    }

    // glViewport(0, 0, Util::GAME_WIDTH, Util::GAME_HEIGHT);
    Res::getFramebuffer("framebuffer").drawFramebuffer();
    // glViewport(0, 0, m_width, m_height);

    //Input::outputGamepadLayout();
    Input::updateGamepadInputs();
    WindowManager::prepareNextLoop();
}