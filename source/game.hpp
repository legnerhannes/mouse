/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#ifndef GAME_H
#define GAME_H

#include "window_manager.hpp"
#include "game_object.hpp"
#include "player.hpp"
#include "model.hpp"
#include "framebuffer.hpp"

// Represents the current state of the game
enum GameState {
    GAME_ACTIVE,
    GAME_MENU,
    GAME_WIN
};

const float PLAYER_VELOCITY(0.1f);

// Game holds all game-related state and functionality.
// Combines all game-related data into a single class for
// easy access to each of the components and manageability.
class Game
{
    private:
        Player *m_player;
        std::vector<Collider> m_colliderInScene;
        std::vector<Prop> m_props;
        glm::vec3 m_lookAt, m_lookAtStart, m_camPos, m_camPosStart;
        
        void processInput();
        void render();

    public:
        // game state
        GameState               State;	
        bool                    Keys[1024];
        unsigned int            m_width, m_height;

        // constructor/destructor
        Game(unsigned int width, unsigned int height);
        ~Game();
        // initialize game state (load all shaders/textures/levels)
        void init();
        // game loop
        void update();
};

#endif