#ifndef SPRITE_3D_HPP
#define SPRITE_3D_HPP

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "shader.hpp"
#include "texture.hpp"

class SpriteCanvas {
    private:
        static float m_angle;

        unsigned int m_quadVAO;
        Shader m_shader;

    public:
        static void setCameraAngle(glm::vec3 cameraDir);

        SpriteCanvas() {};
        SpriteCanvas(int size, Shader shader);

        void setMatrices(glm::mat4 view, glm::mat4 proj);
        void draw(glm::mat4 model, unsigned int texture);
};

#endif