#include "i_res.hpp"

#include <iostream>
#include <sstream>
#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


std::map<std::string, Texture> Res::m_textures;
std::map<std::string, Shader> Res::m_shaders;
std::map<std::string, Model> Res::m_models;
std::map<std::string, SpriteCanvas> Res::m_canvas;
std::map<std::string, Framebuffer> Res::m_framebuffers;
std::map<std::string, LevelLoader> Res::m_level;

Shader Res::loadShaderFromFile(const char *vShaderFile, const char *fShaderFile, const char *gShaderFile)
{
    // 1. retrieve the vertex/fragment source code from filePath
    std::string vertexCode;
    std::string fragmentCode;
    std::string geometryCode;
    try
    {
        // open files
        std::ifstream vertexShaderFile(vShaderFile);
        std::ifstream fragmentShaderFile(fShaderFile);
        std::stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vertexShaderFile.rdbuf();
        fShaderStream << fragmentShaderFile.rdbuf();
        // close file handlers
        vertexShaderFile.close();
        fragmentShaderFile.close();
        // convert stream into string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
        // if geometry shader path is present, also load a geometry shader
        if (gShaderFile != nullptr)
        {
            std::ifstream geometryShaderFile(gShaderFile);
            std::stringstream gShaderStream;
            gShaderStream << geometryShaderFile.rdbuf();
            geometryShaderFile.close();
            geometryCode = gShaderStream.str();
        }
    }
    catch (std::exception e)
    {
        std::cout << "ERROR::SHADER: Failed to read shader files" << std::endl;
    }
    const char *vShaderCode = vertexCode.c_str();
    const char *fShaderCode = fragmentCode.c_str();
    const char *gShaderCode = geometryCode.c_str();
    // 2. now create shader object from source code
    Shader shader;
    shader.Compile(vShaderCode, fShaderCode, gShaderFile != nullptr ? gShaderCode : nullptr);
    return shader;
}

Texture Res::loadTextureFromFile(const char *file, bool alpha, unsigned int filter, unsigned int wrap)
{
    // create texture object
    Texture texture(filter, wrap);
    if (alpha)
    {
        texture.Internal_Format = GL_RGBA;
        texture.Image_Format = GL_RGBA;
    }
    // load image
    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(true);
    unsigned char* data = stbi_load(file, &width, &height, &nrChannels, 0);
    // now generate texture
    texture.Generate(width, height, data);
    // and finally free image data
    stbi_image_free(data);
    return texture;
}

Model Res::loadModelFromFile(const char *file, std::string shader, std::string texture) {
    Model model(file, Res::getShader(shader), Res::getTexture(texture).ID);
    return model;
}

SpriteCanvas Res::loadCanvas(int size, std::string shader) {
    SpriteCanvas canvas(size, Res::getShader(shader));
    return canvas;
}

Framebuffer Res::loadFramebuffer(std::string shader, unsigned int width, unsigned int height) {
    Framebuffer framebuffer(Res::getShader(shader), width, height);
    return framebuffer;
}


std::vector<Texture> Res::loadSpriteSheetFromFile(const char *file, int xAmount, int yAmount, int xSize, int ySize) {
    std::vector<Texture> sprites;

    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(false);
    unsigned char* data = stbi_load(file, &width, &height, &nrChannels, 0);

    // going through each individual sprite
    for (int row = 0; row < yAmount; row++) {
        for (int col = 0; col < xAmount; col++) {
            unsigned char* spriteData = (unsigned char*) malloc(sizeof(unsigned char) * xSize * ySize * nrChannels);

            // going through each individual pixel and copy it
            for (int x = 0; x < xSize; x++) {
                for (int y = 0; y < ySize; y++) {
                    int xPos = col * xSize + x;
                    int yPos = row * ySize + y;

                    int dest = (y * xSize + x) * nrChannels;
                    int source = (yPos * width + xPos) * nrChannels;

                    
                    for (int channel = 0; channel < nrChannels; channel++) {
                        spriteData[dest + channel] = data[source + channel];
                    }
                }
            }


            Texture texture(GL_NEAREST, GL_CLAMP_TO_EDGE);
            if (true)
            {
                texture.Internal_Format = GL_RGBA;
                texture.Image_Format = GL_RGBA;
            }
            texture.Generate(xSize, ySize, spriteData);
            sprites.push_back(texture);
        }
    }
                  
    stbi_image_free(data);
    return sprites;
}



void Res::createShader(const char *vShaderFile, const char *fShaderFile, const char *gShaderFile, std::string name) {
    m_shaders[name] = loadShaderFromFile(vShaderFile, fShaderFile, gShaderFile);
}

Shader Res::getShader(std::string name) {
    return m_shaders[name];
}


void Res::createTexture(const char *file, bool alpha, unsigned int filter, unsigned int wrap, std::string name) {
    m_textures[name] = loadTextureFromFile(file, alpha, filter, wrap);
}

void Res::createSprites(const char *file, int xAmount, int yAmount, int xSize, int ySize, std::string name) {
    std::vector<Texture> sprites = loadSpriteSheetFromFile(file, xAmount, yAmount, xSize, ySize);

    for (int i = 0; i < sprites.size(); i++) {
        m_textures[name + std::to_string(i)] = sprites[i];
    }
}

Texture Res::getTexture(std::string name) {
    return m_textures[name];
}


void Res::createModel(const char *file, std::string shader, std::string texture, std::string name) {
    m_models[name] = loadModelFromFile(file, shader, texture);
}

Model Res::getModel(std::string name) {
    return m_models[name];
}


void Res::createCanvas(int size, std::string shader, std::string name) {
    m_canvas[name] = loadCanvas(size, shader);
}

SpriteCanvas Res::getCanvas(std::string name) {
    return m_canvas[name];
}


void Res::createFramebuffer(std::string shader, unsigned int width, unsigned int height, std::string name) {
    m_framebuffers[name] = loadFramebuffer(shader, width, height);
}

Framebuffer Res::getFramebuffer(std::string name) {
    return m_framebuffers[name];
}

void Res::createLevel(const char* path, std::string name) {
    LevelLoader level(path);
    m_level[name] = level;
}

LevelLoader Res::getLevel(std::string name) {
    return m_level[name];
}



void Res::clearResources()
{
    // (properly) delete all shaders	
    for (auto iter : m_shaders)
        glDeleteProgram(iter.second.ID);
    // (properly) delete all textures
    for (auto iter : m_textures)
        glDeleteTextures(1, &iter.second.ID);
}