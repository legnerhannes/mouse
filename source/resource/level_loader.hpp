#ifndef LEVEL_LOADER_HPP
#define LEVEL_LOADER_HPP

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <string>
#include <vector>

struct PropData {
    std::string modelName, texName;
    glm::vec3 position, scale, rotation;
    std::vector<glm::vec4> collider;
};

class LevelLoader {
    public:
        std::vector<PropData> m_props;

        LevelLoader() {};
        LevelLoader(const char* path);
};

#endif