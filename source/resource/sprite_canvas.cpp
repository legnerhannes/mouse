#include "sprite_canvas.hpp"
#include "util.hpp"

#include <iostream>

float SpriteCanvas::m_angle = 0.0f;

void SpriteCanvas::setCameraAngle(glm::vec3 cameraDir) {
    glm::vec3 forward(0.0f, -1.0f, 0.0f);

    // calc angle in radians
    float dotProduct = glm::dot(cameraDir, forward);
    float magnitude1 = glm::length(cameraDir);
    float magnitude2 = glm::length(forward);
    float cosine = dotProduct / (magnitude1 * magnitude2);
    m_angle = std::acos(cosine); //glm::degrees(std::acos(cosine));
}


SpriteCanvas::SpriteCanvas(int size, Shader shader) : m_shader(shader) {

    float halfDefaultSize = 0.45f; (float) (Util::SCREEN_HEIGHT / Util::GAME_HEIGHT) / 2.0f;
    float halfSize = 0.45f;
    if (size == 30) halfSize = 0.45f;

    float quadVertices[] = {
        // positions         // texture Coords (swapped y coordinates because texture is flipped upside down)
        -halfSize, 0.0f,  halfSize,  0.0f,  0.0f,
        -halfSize, 0.0f, -halfSize,  0.0f,  1.0f,
         halfSize, 0.0f, -halfSize,  1.0f,  1.0f,

        -halfSize, 0.0f,  halfSize,  0.0f,  0.0f,
         halfSize, 0.0f, -halfSize,  1.0f,  1.0f,
         halfSize, 0.0f,  halfSize,  1.0f,  0.0f
    };

    unsigned int VBO;

    glGenVertexArrays(1, &m_quadVAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(m_quadVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
}


void SpriteCanvas::setMatrices(glm::mat4 view, glm::mat4 proj) {
    m_shader.Use();
    m_shader.SetMatrix4("view", view);
    m_shader.SetMatrix4("projection", proj);
}

void SpriteCanvas::draw(glm::mat4 model, unsigned int texture) {
    model = glm::rotate(model, m_angle, glm::vec3(-1.0f, 0.0f, 0.0f));
    
    m_shader.Use();
    m_shader.SetMatrix4("model", model);

    glBindVertexArray(m_quadVAO);
    glBindTexture(GL_TEXTURE_2D, texture);
    glActiveTexture(GL_TEXTURE0);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}