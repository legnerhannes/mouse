#ifndef FRAMEBUFFER_HPP
#define FRAMEBUFFER_HPP

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "shader.hpp"

class Framebuffer {
    private:
        Shader m_shader;
        unsigned int m_quadVAO, m_fbo, m_tex, m_rbo, m_color, m_normal, m_depth, m_sprites;
        unsigned int m_width, m_height;

    public:
        Framebuffer() {};
        Framebuffer(Shader shader, unsigned int width, unsigned int height);

        void bindFramebuffer();
        void drawFramebuffer();
        unsigned int getWidth();
        unsigned int getHeight();
};


#endif