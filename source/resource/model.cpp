#include "happly.h"
#include "model.hpp"

Model::Model(const char *file, Shader shader, unsigned int diffuse) : m_shader(shader), m_diffuseTex(diffuse) {
    happly::PLYData model(file);

    // get positions
    std::vector<float> x = model.getElement("vertex").getProperty<float>("x");
    std::vector<float> y = model.getElement("vertex").getProperty<float>("y");
    std::vector<float> z = model.getElement("vertex").getProperty<float>("z");

    // get normals
    std::vector<float> nx = model.getElement("vertex").getProperty<float>("nx");
    std::vector<float> ny = model.getElement("vertex").getProperty<float>("ny");
    std::vector<float> nz = model.getElement("vertex").getProperty<float>("nz");

    // get uvs
    std::vector<float> u = model.getElement("vertex").getProperty<float>("s");
    std::vector<float> v = model.getElement("vertex").getProperty<float>("t");

    std::vector<std::vector<unsigned int>> indices = model.getElement("face").getListProperty<unsigned int>("vertex_indices");

    m_elementCount = indices.size() * 3;
    unsigned int* vertIndices = (unsigned int*) malloc(sizeof(unsigned int) * m_elementCount);

    for (int i = 0; i < m_elementCount; i += 3) {
        vertIndices[i + 0] = indices[i / 3][0];
        vertIndices[i + 1] = indices[i / 3][1];
        vertIndices[i + 2] = indices[i / 3][2];
    }

    m_vertexCount = model.getElement("vertex").count;
    
    int structSize = 8;
    float* vertBuffer = (float*) malloc(sizeof(float) * m_vertexCount * structSize);

    for (int i = 0; i < m_vertexCount; i++) {
        int realIndex = i * structSize;

        vertBuffer[realIndex + 0] = x[i];
        vertBuffer[realIndex + 1] = y[i];
        vertBuffer[realIndex + 2] = z[i];

        vertBuffer[realIndex + 3] = nx[i];
        vertBuffer[realIndex + 4] = ny[i];
        vertBuffer[realIndex + 5] = nz[i];

        vertBuffer[realIndex + 6] = u[i];
        vertBuffer[realIndex + 7] = v[i];
    }

    unsigned int VBO, EBO;
    glGenVertexArrays(1, &m_modelVAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(m_modelVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * m_vertexCount * structSize, vertBuffer, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_elementCount * sizeof(unsigned int), vertIndices, GL_STATIC_DRAW);


    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * structSize, (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * structSize, (void*) offsetof(Vertex, normal));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(float) * structSize, (void*) offsetof(Vertex, uv));
    glEnableVertexAttribArray(2);

    free(vertBuffer);

    m_shader.Use();
	m_shader.SetVector3f("lightDir", glm::vec3(1.0f, -1.0f, 1.0f));
}

void Model::setMatrices(glm::mat4 view, glm::mat4 proj) {
    m_shader.Use();
    m_shader.SetMatrix4("view", view);
    m_shader.SetMatrix4("projection", proj);
}

void Model::draw(glm::mat4 model) {
    m_shader.Use();
    m_shader.SetMatrix4("model", model);

    glBindVertexArray(m_modelVAO);
    glBindTexture(GL_TEXTURE_2D, m_diffuseTex);
    glActiveTexture(GL_TEXTURE0);
    glDrawElements(GL_TRIANGLES, m_elementCount, GL_UNSIGNED_INT, 0);
}