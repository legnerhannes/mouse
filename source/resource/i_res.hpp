#ifndef RESOURCE_MANAGER_HPP
#define RESOURCE_MANAGER_HPP

#include <map>
#include <string>
#include <vector>

#include "texture.hpp"
#include "shader.hpp"
#include "model.hpp"
#include "sprite_canvas.hpp"
#include "framebuffer.hpp"
#include "level_loader.hpp"


class Res
{
    private:
        static std::map<std::string, Shader> m_shaders;
        static std::map<std::string, Texture> m_textures;
        static std::map<std::string, Model> m_models;
        static std::map<std::string, SpriteCanvas> m_canvas;
        static std::map<std::string, Framebuffer> m_framebuffers;
        static std::map<std::string, LevelLoader> m_level;
        
        // private constructor, that is we do not want any actual resource manager objects. Its members and functions should be publicly available (static).
        Res() { }

        static Shader loadShaderFromFile(const char *vShaderFile, const char *fShaderFile, const char *gShaderFile = nullptr);
        static Texture loadTextureFromFile(const char *file, bool alpha, unsigned int filter, unsigned int wrap);
        static Model loadModelFromFile(const char *file, std::string shader, std::string texture);
        static SpriteCanvas loadCanvas(int size, std::string shader);
        static Framebuffer loadFramebuffer(std::string shader, unsigned int width, unsigned int height);
        static std::vector<Texture> loadSpriteSheetFromFile(const char *file, int xAmount, int yAmount, int xSize, int ySize);

    public:
        static void createShader(const char *vShaderFile, const char *fShaderFile, const char *gShaderFile, std::string name);
        static Shader getShader(std::string name);

        static void createTexture(const char *file, bool alpha, unsigned int filter, unsigned int wrap, std::string name);
        static void createSprites(const char *file, int xAmount, int yAmount, int xSize, int ySize, std::string name);
        static Texture getTexture(std::string name);
        
        static void createModel(const char *file, std::string shader, std::string texture, std::string name);
        static Model getModel(std::string name);

        static void createCanvas(int size, std::string shader, std::string name);
        static SpriteCanvas getCanvas(std::string name);

        static void createFramebuffer(std::string shader, unsigned int width, unsigned int height, std::string name);
        static Framebuffer getFramebuffer(std::string name);

        static void createLevel(const char *file, std::string name);
        static LevelLoader getLevel(std::string name);

        static void clearResources();
    
};

#endif