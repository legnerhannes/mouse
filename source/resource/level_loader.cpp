#include "level_loader.hpp"
#include "json.hpp"

#include <iostream>
#include <fstream>

LevelLoader::LevelLoader(const char* path) {
    std::ifstream levelFile(path);
    if (!levelFile.is_open()) std::cerr << "Failed to open JSON file." << std::endl;
    
    nlohmann::json levelData;

    try {
        levelFile >> levelData;
    } catch (const nlohmann::json::parse_error& e) {
        std::cerr << "JSON parsing error: " << e.what() << std::endl;
    }

    nlohmann::json levelProps = levelData["levelProps"];

    m_props.reserve(levelProps.size());

    for (nlohmann::json prop : levelProps) {
        PropData data;
        data.modelName = prop["modelName"];
        data.texName = prop["texName"];
        data.position = glm::vec3(prop["position"][0], prop["position"][1], prop["position"][2]);
        data.scale = glm::vec3(prop["scale"][0], prop["scale"][1], prop["scale"][2]);
        data.rotation = glm::vec3(prop["rotation"][0], prop["rotation"][1], prop["rotation"][2]);

        for (nlohmann::json collider : prop["collider"]) {
            data.collider.push_back(glm::vec4(collider[0], collider[1], collider[2], collider[3]));
        }

        m_props.push_back(data);
    }
}