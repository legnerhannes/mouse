#ifndef MODEL_HPP
#define MODEL_HPP

#include "shader.hpp"

#include <string>

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 uv;
};

class Model {
    private:
        Shader m_shader;
        unsigned int m_diffuseTex;
        unsigned int m_modelVAO, m_vertexCount, m_elementCount;
        unsigned int *m_vertexIndices;

    public:
        Model() {};
        Model(const char *file, Shader shader, unsigned int diffuse);

        void setMatrices(glm::mat4 view, glm::mat4 proj);
        void draw(glm::mat4 model);
};

#endif
