#ifndef INPUT_HPP
#define INPUT_HPP

#include <vector>

class Input {
    private:
        static std::vector<float> m_axes;
        static std::vector<bool> m_buttons;

    public:
        const unsigned int
            UP = 0,
            DOWN = 1,
            LEFT = 2,
            RIGHT = 3;

        static bool up, down, left, right, interact, attack;

        static void updateGamepadInputs();
        static void mapGamepadInputs();
        static void mapKeyboardInputs();
        static void outputGamepadLayout();
};

#endif