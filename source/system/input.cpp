#include "input.hpp"
#include "window_manager.hpp"

#include <iostream>

std::vector<float> Input::m_axes;
std::vector<bool> Input::m_buttons;

bool Input::up;
bool Input::down;
bool Input::left;
bool Input::right;
bool Input::interact;
bool Input::attack;

void Input::updateGamepadInputs() {
    m_axes.clear();
    m_buttons.clear();
    if (WindowManager::getGamepadInputs(&m_axes, &m_buttons)) mapGamepadInputs();
    else mapKeyboardInputs();

}

void Input::mapGamepadInputs() {
    up = m_buttons[10];
    down = m_buttons[12];
    left = m_buttons[13];
    right = m_buttons[11];

    interact = m_buttons[1];
    attack = m_buttons[0];
}


void Input::mapKeyboardInputs() {
    up = WindowManager::getKey("w");
    left = WindowManager::getKey("a");
    down = WindowManager::getKey("s");
    right = WindowManager::getKey("d");

    interact = WindowManager::getKey("z");
    attack = WindowManager::getKey("x");
}


void Input::outputGamepadLayout() {
    std::cout << "====== AXES ======" << std::endl;
    for (int i = 0; i < m_axes.size(); i++) {
        std::cout << "axis " << i << ": " << m_axes[i] << std::endl;
    }

    std::cout << "====== BUTTONS ======" << std::endl;
    for (int i = 0; i < m_buttons.size(); i++) {
        std::string state = "";

        if (m_buttons[i]) state = "O";
        else state = "X";

        std::cout << "button " << i << ": " << state << std::endl;
    }
}