#ifndef WINDOW_MANAGER_HPP
#define WINDOW_MANAGER_HPP

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string>
#include <vector>
#include <map>

class WindowManager {
    private:
        static GLFWwindow *m_window;
        static std::map<std::string, bool> m_keys;

        static void framebufferSizeCallback(GLFWwindow* window, int width, int height);
        static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);

    public:
        static void init(int width, int height);
        static float getTime();
        static bool close();
        static bool getKey(std::string key);
        static bool getGamepadInputs(std::vector<float>* axes, std::vector<bool>* buttons);
        static void prepareNextLoop();
        static void setupKeys();
};

#endif