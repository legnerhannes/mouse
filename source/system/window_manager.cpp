#include "window_manager.hpp"
#include "util.hpp"

#include <iostream>


GLFWwindow *WindowManager::m_window;
std::map<std::string, bool> WindowManager::m_keys;

void WindowManager::framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}


void WindowManager::setupKeys() {
    m_keys.insert(std::pair<std::string, bool>("w", false));
    m_keys.insert(std::pair<std::string, bool>("a", false));
    m_keys.insert(std::pair<std::string, bool>("s", false));
    m_keys.insert(std::pair<std::string, bool>("d", false));
    m_keys.insert(std::pair<std::string, bool>("z", false));
    m_keys.insert(std::pair<std::string, bool>("x", false));
}

bool WindowManager::getKey(std::string key) {
    return m_keys[(std::string) key];
}


void WindowManager::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    switch (key) {
        case GLFW_KEY_W:
            if (action == GLFW_PRESS) m_keys["w"] = true;
            if (action == GLFW_RELEASE) m_keys["w"] = false;
            break;

        case GLFW_KEY_A:
            if (action == GLFW_PRESS) m_keys["a"] = true;
            if (action == GLFW_RELEASE) m_keys["a"] = false;
            break;

        case GLFW_KEY_S:
            if (action == GLFW_PRESS) m_keys["s"] = true;
            if (action == GLFW_RELEASE) m_keys["s"] = false;
            break;

        case GLFW_KEY_D:
            if (action == GLFW_PRESS) m_keys["d"] = true;
            if (action == GLFW_RELEASE) m_keys["d"] = false;
            break;

        case GLFW_KEY_Z:
            if (action == GLFW_PRESS) m_keys["z"] = true;
            if (action == GLFW_RELEASE) m_keys["z"] = false;
            break;

        case GLFW_KEY_X:
            if (action == GLFW_PRESS) m_keys["x"] = true;
            if (action == GLFW_RELEASE) m_keys["x"] = false;
            break;
    }
}

void WindowManager::init(int width, int height) {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, false);

    m_window = glfwCreateWindow(width, height, "Mouse Game", nullptr, nullptr);
    glfwMakeContextCurrent(m_window);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return;
    }

    glfwSetFramebufferSizeCallback(m_window, framebufferSizeCallback);
    glfwSetKeyCallback(m_window, keyCallback);
    
    glViewport(0, 0, width, height);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glDepthFunc(GL_LESS);

    setupKeys();
}

bool WindowManager::close() {
    if (glfwWindowShouldClose(m_window)) {
        glfwTerminate();
        return true;
    }
    else return false;
}

float WindowManager::getTime() {
    return glfwGetTime();
}

void WindowManager::prepareNextLoop() {
    glfwPollEvents();
    glfwSwapBuffers(m_window);
}

bool WindowManager::getGamepadInputs(std::vector<float>* axes, std::vector<bool>* buttons) {
    int gamepadIndex = 0;

    if (glfwJoystickPresent(gamepadIndex))
    {
        int axisCount, buttonCount;
        const float *tempAxes = glfwGetJoystickAxes(gamepadIndex, &axisCount);
        const unsigned char *tempButtons = glfwGetJoystickButtons(gamepadIndex, &buttonCount);

        for (int i = 0; i < axisCount; i++) {
            axes -> push_back(tempAxes[i]);
        }

        for (int i = 0; i < buttonCount; i++) {
            if (tempButtons[i] == GLFW_PRESS) buttons -> push_back(true);
            if (tempButtons[i] == GLFW_RELEASE) buttons -> push_back(false);
        }

        return true;
    }
    else return false;
}
