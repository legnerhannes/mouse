#ifndef UTIL_HPP
#define UTIL_HPP

#include <vector>

class Util {
    public:
        static const unsigned int SCREEN_WIDTH = 1280, SCREEN_HEIGHT = 720;
        static const unsigned int GAME_WIDTH = 1280 / 3, GAME_HEIGHT = 720 / 3;

        template <typename T>
        static std::vector<T> getVectorFromArray(T* array, int size);
};

#endif