#include "game.hpp"
#include "i_res.hpp"
#include "window_manager.hpp"
#include "model.hpp"
#include "util.hpp"

#include <iostream>

const unsigned int SCREEN_WIDTH = 384;
const unsigned int SCREEN_HEIGHT = 216;

int main()
{
    Game mouse(Util::SCREEN_WIDTH, Util::SCREEN_HEIGHT);
    WindowManager::init(Util::SCREEN_WIDTH, Util::SCREEN_HEIGHT);
    
    mouse.init();

    float deltaTime = 0.0f;
    float lastFrame = 0.0f;

    float updateTimer = 1.0f / 60.0f;
    float lastUpdate = 0.0f;

    while (!WindowManager::close()) {
        float currentFrame = WindowManager::getTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;


        if (currentFrame - lastUpdate >= updateTimer) {
            mouse.update();
            lastUpdate = currentFrame;
        }
    }

    return 0;
}

